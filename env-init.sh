k3d cluster create flux-cd-test
k3d kubeconfig merge flux-cd-test --kubeconfig-switch-context

GITLAB_USER="piximos"
GITLAB_REPOSITORY="git@gitlab.com:piximos/flux-cd-test.git"

kubectl create ns flux-cd
fluxctl install \
--git-user="$GITLAB_USER" \
--git-email="dhraief.youssef@gmail.com" \
--git-url="$GITLAB_REPOSITORY" \
--git-path=kube-src \
--git-branch=master \
--namespace=flux-cd | kubectl apply -f -